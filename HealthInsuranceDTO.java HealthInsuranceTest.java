
public class HealthInsuranceTest {
	
	public static void main(String args[]) {
		// TODO Auto-generated method stub
		HealthInsuranceDTO dto = new HealthInsuranceDTO();
		dto.setFirstName("Gomes");
		dto.setLastName("Norman");
		dto.setGender("Male");
		dto.setAge(34);
		dto.setHyperTension("no");
		dto.setBloodPressure("no");
		dto.setBloodSugar("no");
		dto.setOverweight("Yes");
		dto.setAlcohol("Yes");
		dto.setDrugs("No");
		dto.setSmoking("No");
		dto.setDailyExercise("Yes");
		dto.setBasicInsuranceAmt(5000);
		if(dto.getAge() >= 18 ) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*10/100);
		}if(dto.getAge() >= 25 ) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*10/100);
		}if(dto.getAge() >= 30 ) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*10/100);
		}
		if(dto.getAge() >= 35 ) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*10/100);
		}
		//....we can add all the remaining conditions here.
		if(dto.getGender().equals("Male")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*2/100);
		}
		if(dto.getHyperTension().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*1/100);
		}
		if(dto.getBloodPressure().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*1/100);
		}
		if( dto.getBloodSugar().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*1/100);
		}
		if(dto.getOverweight().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*1/100);
		}if(dto.getAlcohol().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*3/100);
		}
		if(dto.getDrugs().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*3/100);
		}
		if(dto.getSmoking().equals("yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					+ dto.getBasicInsuranceAmt()*3/100);
		}
		if(dto.getDailyExercise().equals("Yes")) {
			dto.setBasicInsuranceAmt(dto.getBasicInsuranceAmt() 
					- dto.getBasicInsuranceAmt()*3/100);
		}
		
		System.out.println("Health Insurance Premium for Mr." +dto.getFirstName() 
		+ ": " +dto.getBasicInsuranceAmt());
	}import java.util.List;

public class HealthInsuranceDTO {
	private String firstName;
	private String lastName;
	private String gender;
	private int basicInsuranceAmt;
	private int age;
	private String habbits;
	private String currentHealth;
	private String hyperTension;
	private String bloodPressure;
	private String bloodSugar;
	private String overweight;
	private String smoking;
	private String alcohol;
	private String dailyExercise;
	private String drugs;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHabbits() {
		return habbits;
	}
	public void setHabbits(String habbits) {
		this.habbits = habbits;
	}
	public String getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(String currentHealth) {
		this.currentHealth = currentHealth;
	}
	public String getHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	public int getBasicInsuranceAmt() {
		return basicInsuranceAmt;
	}
	public void setBasicInsuranceAmt(int basicInsuranceAmt) {
		this.basicInsuranceAmt = basicInsuranceAmt;
	}
	
}


}


